/*
 * Copyright (C) 2018 Philip M. Trenwith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package LuceneIndexer;

import java.io.File;

/**
 *
 * @author Philip M. Trenwith
 */
public class cUtilities
{

  /**
   * Verify if the location is an absolute path. If it is, return it as is, if it is not, add the working directory to the front
   * @param sLocation The absolute path for this location
   */
  public static String verifyLocation(String sLocation)
  {
    File file = new File(sLocation);
    if (file.exists())
    {
      return file.getAbsolutePath();
    }
    else
    {
      String workingDir = System.getProperty("user.dir");
      file = new File(workingDir += File.separator + sLocation);
      return file.getAbsolutePath();
    }
  }
  
}
